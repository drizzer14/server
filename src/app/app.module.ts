import { server } from 'lib/service'

import { proceduresController } from 'modules/procedures'
import { registriesController } from 'modules/registries'

export const app = server({
  controllers: [
    ['PROCEDURES', proceduresController],
    ['REGISTRIES', registriesController]
  ]
})
