export const dotenv = (): void => {
  switch (process.env.NODE_ENV) {
    case 'development': {
      return require('dotenv').config()
    }
    case 'test': {
      return require('dotenv').config({
        path: './.env.test'
      })
    }
    default: {
      return
    }
  }
}
