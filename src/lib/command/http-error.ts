export class HTTPError extends Error {
  public readonly name = 'HTTPError'

  public constructor(
    public readonly statusCode: number,
    public readonly message: string
  ) {
    super(message)
  }
}
