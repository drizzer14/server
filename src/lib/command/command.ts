import axios, { AxiosError } from 'axios'
import { either, Either } from 'fnts/monad/either'

import { HTTPError } from './http-error';

export type Command<Data> = (
  url: string,
  query: object
) => Promise<Either<HTTPError, Data>>

export const command =
  <Response, Data>(transformer: (response: Response) => Data): Command<Data> =>
  async (url, query) => {
    return either<never, Data>(
      (error) => {
        const { code = '500', message } = error as AxiosError

        throw new HTTPError(
          Number(code),
          message
        )
      },
      async () => {
        const { data } = await axios.post<Response>(
          url,
          query
          );

        return transformer(data)
      }
    )
  }
