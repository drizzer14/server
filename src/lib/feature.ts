import { eq } from 'fnts/boolean'

import { dotenv } from './dotenv'

dotenv()

type FeaturesConfig = {
  REGISTRIES: boolean
  PROCEDURES: boolean
}

const parseBoolean = eq<string | undefined>('true')

export type Feature = (param: keyof FeaturesConfig) => boolean

export const feature: Feature = (param) => {
  return {
    REGISTRIES: parseBoolean(process.env.FF_REGISTRIES),
    PROCEDURES: parseBoolean(process.env.FF_PROCEDURES)
  }[param]
}
