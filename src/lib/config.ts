import { dotenv } from './dotenv'

dotenv()

type AppConfig = {
  SERVICE_HOST: string
  SERVICE_PORT: number
  ELASTIC_URL: string
}

export type Config = <Param extends keyof AppConfig>(
  param: Param
) => AppConfig[Param]

export const config: Config = (param) => {
  return {
    SERVICE_HOST: process.env.SERVICE_HOST!,
    SERVICE_PORT: Number(process.env.SERVICE_PORT),
    ELASTIC_URL: process.env.ELASTIC_URL!
  }[param]
}
