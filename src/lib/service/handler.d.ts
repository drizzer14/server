import type { Logger } from 'pino'
import type { Request as HTTPRequest, Response } from '@tinyhttp/app'
import type { Method as RequestMethod, NextFunction } from '@tinyhttp/router'

import type { Config } from 'lib/config'
import type { Feature } from 'lib/feature'

type Request<Method extends RequestMethod, Params extends string> = Omit<HTTPRequest, 'params' | 'method'> & {
  method: Method
  params: {
    [param in Params]: string
  }
}

type ParsePathParams<Path> = Path extends `${string}/:${infer P}/${infer Rest}`
  ? P | ParsePathParams<`/${Rest}`>
  : Path extends `${string}/:${infer P}`
    ? P
    : undefined

export type HandlerContext = {
  config: Config
  feature: Feature
  logger: Logger
}

export type Handler<Method extends RequestMethod, Path extends string = string> =
  (context: HandlerContext) =>
  (
    request: Request<Method, ParsePathParams<Path>>,
    response: Response,
    next: NextFunction
  ) => void | Promise<void>
