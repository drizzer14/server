import { snd } from 'fnts/pair'
import { foldl } from 'fnts/array'
import type { App } from '@tinyhttp/app'
import type { Server as HTTPServer } from 'http'

import { Feature } from 'lib/feature'

import { Middleware, use } from './middleware'
import type { Controller } from './controller'
import type { HandlerContext } from './handler'

export type ServerContext = {
  app: App
} & HandlerContext

export type Server = (context: ServerContext) => HTTPServer

export const server = (
  {
    controllers,
    middlewares = []
  }: {
    controllers: Array<[Parameters<Feature>[0] | boolean, Controller]>
    middlewares?: Middleware[]
  }
): Server =>
  ({ app, ...context }) => {
    const [
      port,
      host
    ] = (
      ['SERVICE_PORT', 'SERVICE_HOST'] as const
    ).map(context.config) as [number, string]

    const logger = context.logger.child({ name: 'host' })

    return foldl<Controller, App>(
      (target, controller) => controller({
        app: target,
        ...context
      }),
      use(middlewares, context)(app)
    )(
      controllers
        .filter(([isEnabled]) => {
          return typeof isEnabled === 'boolean'
            ? isEnabled
            : context.feature(isEnabled)
        })
        .map(snd)
    )
      .listen(
        port,
        () => {
          logger.info(`Started server -> http://${host}:${port}`)
        },
        host
      )
}
