import type { App } from '@tinyhttp/app'
import type { Method as RequestMethod, AsyncHandler } from '@tinyhttp/router'
import { logger } from '../logger'

import type { Handler, HandlerContext } from './handler'

export type RouteContext = {
  add: App['add']
  basePath: string
} & HandlerContext

export type Route = (
  context: RouteContext
) => App

export const route =
  <Method extends RequestMethod, Path extends string = string>(
    method: Method, path = '' as Path
  ) =>
  (handler: Handler<Method, Path>): Route =>
  ({ add, basePath, ...context }) => {
    const routePath = `${basePath}${basePath && path === '' ? '/' : path}`
    const logger = context.logger.child({ name: routePath })

    logger.info(`Applied route`)

    return add(method)(
      routePath,
      (request, _, next) => {
        logger.info(`${method} ${request.originalUrl}`)

        next()
      },
      handler({ ...context, logger }) as AsyncHandler,
    )
  }
