import { foldl } from 'fnts/array'
import type { App } from '@tinyhttp/app'
import type { AsyncHandler, Method as RequestMethod } from '@tinyhttp/router'

import type { Handler, HandlerContext } from './handler'

export type MiddlewareContext = {
  use: App['use']
} & HandlerContext

export type Middleware = (context: MiddlewareContext) => App

export const middleware =
  <Path extends string = string>(path: Path) =>
  (handler: Handler<RequestMethod, Path>): Middleware =>
  ({ use, ...handlerContext }) => use(path, handler(handlerContext) as AsyncHandler)

export const use = (middlewares: Middleware[], context: HandlerContext) =>
  (app: App): App => {
    return foldl<Middleware, App>(
      (target, middleware) => {
        return middleware({
          use: target.use.bind(target),
          ...context
        })
      },
      app
    )(middlewares)
  }
