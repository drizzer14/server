import { foldl } from 'fnts/array'
import type { App } from '@tinyhttp/app'
import { logger } from '../logger'
import { use } from './middleware'

import type { Route } from './route'
import type { Middleware } from './middleware'
import type { HandlerContext } from './handler'

export type ControllerContext = {
  app: App
} & HandlerContext

export type Controller = (context: ControllerContext) => App

export const controller = (
  path: string,
  {
    routes,
    middlewares = []
  }: {
    routes: Route[]
    middlewares?: Middleware[]
  }
): Controller => ({ app, ...context }) => {
  const logger = context.logger.child({ name: path })

  logger.info(`Instantiated controller`)

  return foldl<Route, App>(
    (target, route) => {
      return route({
        add: target.add.bind(target),
        basePath: path,
        ...context,
      })
    },
    use(middlewares, context)(app)
  )(routes)
}
