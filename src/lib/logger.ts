import pino from 'pino'

export const logger = pino({
  prettyPrint: {
    colorize: true,
    levelFirst: true,
    ignore: 'hostname,pid,name',
    translateTime: 'yyyy-mm-dd HH:MM:ss:l',
    messageFormat: ({ msg, name = '' }) => `${name && `${name} -> `}${msg}`
  },
  level: process.env.NODE_ENV === 'production' ? 'debug' : 'trace',
})

