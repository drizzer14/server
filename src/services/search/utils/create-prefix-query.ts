export const createPrefixQuery = (key: string, value: string, options = {} as Record<string, unknown>) => ({
  match_phrase_prefix: {
    [key]: {
      query: value,
      ...options
    }
  }
})
