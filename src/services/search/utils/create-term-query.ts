export const createTermQuery = (key: string, value: string | string[]): object => {
  return {
    [Array.isArray(value) && value.length > 1 ? 'terms' : 'term']: {
      [key]: Array.isArray(value) && value.length === 1 ? value[0] : value,
    },
  }
}
