import { getUnique } from './get-unique'

export const joinQueryString = (value: string[], operator: 'AND' | 'OR'): string => {
  const jointQueries = getUnique(value).map((query) => query.split(' ').join(' AND '))

  return (jointQueries.length > 1 ? jointQueries.map((query) => `"${query}"`) : jointQueries).join(` ${operator} `)
}
