export const createStringQuery = (keys: string | string[], value: string, options = {} as Record<string, unknown>) => ({
  query_string: {
    query: value,
    [Array.isArray(keys) && keys.length > 1 ? 'fields' : 'default_field']: keys,
    ...options,
  },
})
