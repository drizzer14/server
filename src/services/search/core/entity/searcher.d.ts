import type { Query } from './query'

export type Searcher<Param> =
  (query: Partial<Query<Param>>) => Record<string, unknown>
