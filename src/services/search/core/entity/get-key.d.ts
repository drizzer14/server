export type GetKey<Param = string> = (param: Param) => string | string[]
