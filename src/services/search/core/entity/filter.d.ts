import type { Maybe } from 'fnts/monad/maybe'

import type { Context } from './mapper'
import type { Payload } from './payload'

export type Filter<Param = string, Key extends string | string[] = string> = (
  context: Context<Param, Key>
) => Maybe<Payload>
