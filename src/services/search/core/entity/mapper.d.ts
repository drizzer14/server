import type { Maybe } from 'fnts/monad/maybe'

import type { Query } from './query'
import type { Payload } from './payload'

export type Context<Param = string, Key extends string | string[] = string> = {
  query: Query<Param>
  param: Param
  key: Key
  value: string[]
}

export type Mapper<Param = string, Key extends string | string[] = string> = (
  context: Context<Param, Key>
) => Maybe<Payload>
