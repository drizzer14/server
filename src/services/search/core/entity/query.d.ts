export type QueryValue = string | string[]

export type Query<Param = string> = Record<Param, QueryValue>
