export type Payload = {
  type: 'must' | 'must_not'
  payload: object
}
