import { just, nothing } from 'fnts/monad/maybe'

import type { Context, Filter, Payload } from './entity'

type CreatePayloadMapper<K extends string | string[] = string> = (
  context: Omit<Context<string, K>, 'value'> & { value: string }
) => object | null

const createPayload = <K extends string | string[] = string>(type: Payload['type']) => (
  mapper: CreatePayloadMapper<K>
): Filter<string, K> => ({ query, param, key, value: values }) => {
  // eslint-disable-next-line immutable/no-let
  const payload: object | null =
    values.length === 1
      ? mapper({ query, param, key, value: values[0]! })
      : {
          bool: {
            should: values.map((value) => mapper({ query, param, key, value })),
          },
        }

  return payload
      ? just({
          type,
          payload,
        })
      : nothing()
}

export const createMustPayload = createPayload('must')

export const createMustNotPayload = createPayload('must_not')
