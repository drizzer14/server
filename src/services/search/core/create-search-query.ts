import { foldl } from 'fnts/array'
import { split } from 'fnts/string'
import type { Pair } from 'fnts/pair'
import { concat } from 'fnts/array-like'

import { getUnique } from 'services/search/utils'

import type { GetKey, Mapper, Query, QueryValue, Searcher } from './entity'

type SearchQuery = {
  size?: number
  sort?: {
    date: {
      order: 'desc'
    }
  }
  query: {
    bool: {
      must: object[]
      must_not: object[]
    }
  }
}

export const createSearchQuery =
  <Param = string>(getKey: GetKey<Param>, mapper: Mapper<Param>): Searcher<Param> => {
    return (query) => {
      return foldl<[unknown, QueryValue], SearchQuery>(
        (target, [param, value]) => {
          switch (param) {
            case 'size': {
              return Object.assign(target, {
                size: Number(value),
              })
            }
            case 'sort': {
              const [key, order] = split(':')(value as string) as Pair<string>

              return Object.assign(target, {
                sort: {
                  [key]: {
                    order,
                  },
                },
              })
            }
            default: {
              const mapping = mapper({
                query: query as Query<Param>,
                param: param as Param,
                // Weird but ok
                key: getKey(param as Param) as string & string[],
                value: Array.isArray(value) ? getUnique(value) : [value],
              })

              return mapping.foldMap(({ type, payload }) => {
                return Object.assign(target, {
                  query: {
                    bool: Object.assign(
                      target.query.bool,
                      {
                        [type]: concat(payload)(target.query.bool[type])
                      }
                    )
                  }
                })
              }) || target
            }
          }
        },
        {
          query: {
            bool: {
              must: [],
              must_not: [],
            },
          },
        }
      )(
        Object.entries(query)
      )
  }
}
