import { map } from 'fnts/array'

import { command } from 'lib/command'

type Hit = {
  _source: object
}

type SearchResponse = {
  hits: {
    total: {
      value: number
    }
    hits: Hit[]
  }
}

export type SearchResult = {
  data: object[]
  total: number
}

export const searchCommand = command<SearchResponse, SearchResult>(
  ({ hits: { hits, total: { value: total } } }) => ({
    data: map<Hit, object>(
      ({ _source }) => _source
    )(hits),
    total
  })
)
