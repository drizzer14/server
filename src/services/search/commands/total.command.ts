import { command } from '../../../lib/command'

export type TotalResponse = {
  count: number
}

export const totalCommand = command<TotalResponse, number>(
  ({ count }) => count
)
