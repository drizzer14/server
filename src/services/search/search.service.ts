import { eitherS } from 'fnts/monad/either'

import { HTTPError } from 'lib/command'
import type { HandlerContext } from 'lib/service'

import type { Query, Searcher } from './core/entity'
import { searchCommand, SearchResult, totalCommand } from './commands'

export const search =
  ({ logger }: HandlerContext) =>
  (url: string, { offset, ...query }: Query) =>
  async <Param>(searcher: Searcher<Param>): Promise<SearchResult> => {
    const size = query.size || `${Number(offset ?? 0) + 10}`

    const elasticQuery = eitherS(
      (error) => {
        throw new HTTPError(
          500,
          `Internal server error -> ${(error as Error).message}`
        )
      },
      () => searcher({ ...query, size, sort: 'date:desc' } as any)
    ).fold()

    logger.trace(
      `Prepared search query:\n${JSON.stringify(
        elasticQuery, 
        null,
        2
      )}`
    )

    const searchResult = await searchCommand(
      `${url}/_search`,
      elasticQuery
    )

    return searchResult.foldMap(
      (searchError) => {
        throw searchError
      },
      async ({ data, total }) => {
        switch (total) {
          case 0: {
            throw new HTTPError(404, 'Nothing was found.')
          }
          case 10000: {
            if (!query.size) {
              const { size: _size, sort: _sort, ...totalQuery } = elasticQuery

              const totalResult = await totalCommand(
                `${url}/_count`,
                totalQuery
              )

              return totalResult.foldMap(
                (totalError) => {
                  throw totalError
                },
                (count) => ({
                  data,
                  total: count
                })
              )
            }
          }
          default: {
            return {
              data,
              total
            }
          }
        }
      }
    )
  }
