import { App } from '@tinyhttp/app'

import { config } from 'lib/config'
import { logger } from 'lib/logger'
import { feature } from './lib/feature'

import { app } from './app'

app({
  app: new App(),
  config,
  feature,
  logger
})
