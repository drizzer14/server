import { searchRegistries } from '../search'
import { searchRoute } from '../../common/routes'

export const indexRoute = searchRoute({
  name: 'registries',
  baseUrl: 'registry',
  searcher: searchRegistries
})
