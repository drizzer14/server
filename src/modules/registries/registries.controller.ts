import { controller } from 'lib/service'

import * as routes from './routes'

export const registriesController = controller('registries', {
  routes: Object.values(routes)
})
