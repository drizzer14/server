import { createSearchQuery } from 'services/search/core'

import { registriesMapper } from './registries-mapper'
import { getRegistriesKey } from './get-registries-key'

export const searchRegistries = createSearchQuery(getRegistriesKey, registriesMapper)
