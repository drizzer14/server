import type { GetKey } from 'services/search/core/entity'

import type { RegistriesParam } from './entity'

export const getRegistriesKey: GetKey<RegistriesParam> = (param) => {
  switch (param) {
    case 'source':
    case 'assetType': {
      return 'object.mainPropertyCategory'
    }
    case 'status': {
      return 'object.status'
    }
    case 'listType': {
      return 'resolutions.disposalMethod'
    }
    case 'propertyOwner':
    case 'seller': {
      return 'parties.identifier.id'
    }
    case 'cav':
    case 'cav-ps':
    case 'cpv':
    case 'cpvs': {
      return 'object.items'
    }
    case 'value': {
      return 'object.value.amount'
    }
    case 'item_region': {
      return 'object.items.propertyAddress'
    }
    case 'query': {
      return ['object.items.description', 'object.title', 'object.description']
    }
    default: {
      return ''
    }
  }
}
