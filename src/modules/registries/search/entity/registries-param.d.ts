import type { CommonParam } from 'modules/common/entity'

export type RegistriesParam = CommonParam | 'source' | 'listType' | 'assetType' | 'status' | 'seller' | 'propertyOwner'
