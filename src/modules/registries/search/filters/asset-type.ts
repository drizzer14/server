import { just } from 'fnts/monad/maybe'

import { createTermQuery } from 'services/search/utils'
import type { Filter } from 'services/search/core/entity'

export const assetType: Filter = ({ key, value }) => {
  return just({
    type: 'must',
    payload: createTermQuery(key, value),
  })
}
