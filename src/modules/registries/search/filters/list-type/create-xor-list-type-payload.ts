import { createTermQuery } from 'services/search/utils'

export const createXorListTypePayload = (key: string, value: string) => ({
  bool: {
    should: [
      {
        bool: {
          must_not: [
            {
              exists: {
                field: key,
              },
            },
          ],
        },
      },
      {
        bool: {
          must: createTermQuery(key, [value, 'Undefined']),
        },
      },
    ],
  },
})
