import { just, nothing } from 'fnts/monad/maybe'

import { createTermQuery } from 'services/search/utils'
import type { Filter } from 'services/search/core/entity'

import { createXorListTypePayload } from './create-xor-list-type-payload'

export const listType: Filter = ({ key, value }) => {
  switch (`${['First', 'Second', 'Undefined'].map((type) => Number(value.includes(type))).join('')}`) {
    case '100':
    case '010': {
      return just({
        type: 'must',
        payload: createTermQuery(key, value[0]!),
      })
    }
    case '110': {
      return just({
        type: 'must',
        payload: createTermQuery(key, value),
      })
    }
    case '001': {
      return just({
        type: 'must_not',
        payload: createTermQuery(key, ['First', 'Second']),
      })
    }
    case '101': {
      return just({
        type: 'must',
        payload: {
          bool: {
            must: [createXorListTypePayload(key, 'First')],
            must_not: [createTermQuery(key, 'Second')],
          },
        },
      })
    }
    case '011': {
      return just({
        type: 'must',
        payload: {
          bool: {
            must: [createXorListTypePayload(key, 'Second')],
            must_not: [createTermQuery(key, 'First')],
          },
        },
      })
    }
    case '111':
    default: {
      return nothing()
    }
  }
}
