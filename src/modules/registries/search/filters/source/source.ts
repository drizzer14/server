import { just, nothing } from 'fnts/monad/maybe'

import { createTermQuery } from 'services/search/utils'
import type { Filter } from 'services/search/core/entity'

import registriesTypes from './registries-types.json'

import { getRegistriesKey } from '../../get-registries-key'

export const source: Filter = ({ query, key, value: [value] }) => {
  const statusKey = getRegistriesKey('status') as string

  switch (value) {
    case 'dgf': {
      return just({
        type: 'must',
        payload: createTermQuery(statusKey, 'pending'),
      })
    }
    case 'lease': {
      return just({
        type: 'must',
        payload: createTermQuery(key, registriesTypes.lease),
      })
    }
    case 'privatization': {
      return just(
        query.status
          ? {
            type: 'must_not',
            payload: createTermQuery(key, Object.values(registriesTypes).flat()),
          }
          : {
            type: 'must',
            payload: createTermQuery(statusKey, ['pending', 'active']),
          }
      )
    }
    default: {
      return nothing()
    }
  }
}
