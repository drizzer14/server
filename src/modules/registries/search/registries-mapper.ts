import { commonMapper } from 'modules/common'
import { organization } from 'modules/common/filters'
import type { CommonParam } from 'modules/common/entity'
import type { Context, Mapper } from 'services/search/core/entity'

import * as filters from './filters'
import type { RegistriesParam } from './entity'

export const registriesMapper: Mapper<RegistriesParam> = (context) => {
  switch (context.param) {
    case 'source':
    case 'status':
    case 'listType':
    case 'assetType': {
      return filters[context.param](context)
    }
    case 'seller':
    case 'propertyOwner': {
      return organization(context)
    }
    default: {
      return commonMapper(context as Context<CommonParam>)
    }
  }
}
