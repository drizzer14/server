import { either } from 'fnts/monad/either'

import { route } from 'lib/service'
import { HTTPError } from 'lib/command'
import { search } from 'services/search'
import type { Query, Searcher } from 'services/search/core/entity'

type SearchRouteConfig<Param> = {
  name: string
  baseUrl?: string
  searcher: Searcher<Param>
}

export const searchRoute = <Param>({ name, baseUrl = name, searcher }: SearchRouteConfig<Param>) => route('GET')(
  (context) =>
    async (
      {
        method,
        originalUrl,
        query: { [`/${name}`]: _, ...query }
      },
      response
    ) => {
      const { config, logger } = context

      await either(
        (error) => {
          const { statusCode, message } = error as HTTPError

          logger.error(
            `${method} ${originalUrl} -> Error ${statusCode} -> Failed to get ${name} -> ${message}`
          )

          return response
            .status(statusCode)
            .send(message)
        },
        async () => {
          const url = `${config('ELASTIC_URL')}/${baseUrl}`

          logger.debug(
            `Searching for ${name} -> ${url}:\n${JSON.stringify(query, null, 2)}`
          )

          const result = await search
            (context)
            (url, query as Query)
            (searcher)

          logger.info(`${method} ${originalUrl} -> Success 200 -> Found ${result.total} ${name}.`)

          return response
            .header('Content-Type', 'application/json')
            .status(200)
            .send(JSON.stringify(result))
        }
      )
    }
)
