import { createMustPayload } from 'services/search/core'

import { getValuesRange } from './get-values-range'

export const value = createMustPayload(({ key, value: range }) => ({
  bool: {
    filter: {
      range: {
        [key]: getValuesRange(range),
      },
    },
  },
}))
