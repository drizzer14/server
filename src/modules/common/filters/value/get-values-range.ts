export const getValuesRange = (range: string): { gte: number; lte?: number } => {
  const [gte, lte] = range.split('–').map(Number) as [number, number]

  return {
    gte,
    lte,
  }
}
