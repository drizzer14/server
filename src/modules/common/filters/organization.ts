import { createTermQuery } from 'services/search/utils'
import { createMustPayload } from 'services/search/core'

export const organization = createMustPayload(({ param: role, key, value }) => ({
  bool: {
    must: [createTermQuery('parties.roles', role), createTermQuery(key, value)],
  },
}))
