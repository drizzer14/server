import { just } from 'fnts/monad/maybe'

import type { Filter } from 'services/search/core/entity'
import { createStringQuery, joinQueryString } from 'services/search/utils'

export const query: Filter<string[]> = ({ key: keys, value }) => {
  return just({
    type: 'must',
    payload: createStringQuery(keys, joinQueryString(value, 'OR')),
  })
}
