export const getPostalCodesRange = (range: string): { gte: string; lte: string } => {
  const [gte, lte] = range.split('–')

  return { gte: `${gte}000`, lte: `${lte}999` }
}
