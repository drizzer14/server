export const matchRegionName = (region: string): string => {
  const matchesRegion = region.match(/([ЇА-яіїҐґ]+)[зсц]ька область/) as [any, string] | null
  const matchesCity = region.match(/місто ([ЇА-яіїҐґ]+)/) as [any, string] | null

  return [matchesRegion, matchesCity].reduce((accumulator, current) => {
    return current ? current[1] : accumulator
  }, region)
}
