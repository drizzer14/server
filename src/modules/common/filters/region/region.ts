import { createMustPayload } from 'services/search/core'
import { createStringQuery, joinQueryString } from 'services/search/utils'

import regions from './regions.json'
import { matchRegionName } from './match-region-name'
import { getPostalCodesRange } from './get-postal-codes-range'

export const region = createMustPayload(({ key, value }) => {
  const { name } = regions.find(({ id }) => id === value)!

  return {
    bool: {
      should: [
        {
          bool: {
            filter: {
              range: {
                [`${key}.postalCode`]: getPostalCodesRange(value),
              },
            },
          },
        },
        {
          bool: {
            must: createStringQuery(`${key}.region`, joinQueryString([matchRegionName(name), name], 'OR'), {
              fuzziness: 0,
            }),
          },
        },
      ],
    },
  }
})
