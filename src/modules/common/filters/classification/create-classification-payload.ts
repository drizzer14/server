import { createPrefixQuery, createTermQuery } from 'services/search/utils'

export const createClassificationPayload = (key: string, schemes: string | string[], ids: string | string[]): object => ({
  bool: {
    must: [
      createTermQuery(`${key}.scheme`, schemes),
      {
        bool: {
          must: (Array.isArray(ids) ? ids : [ids]).map((id) => {
            return createPrefixQuery(
              `${key}.id`,
              id,
              {
              max_expansions: 10 - id.length
            })
          })
        }
      }
    ],
  },
})
