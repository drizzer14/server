import { just } from 'fnts/monad/maybe'

import type  { Filter } from 'services/search/core/entity'

import { taxonomifyClassificator } from './taxonomify-classificator'
import { createClassificationPayload } from './create-classification-payload'

export const classification: Filter = ({ param, key, value: ids }) => {
  const classificationKey = (() => {
    switch (param) {
      case 'cav':
      case 'cpv': {
        return `${key}.classification`
      }
      default: {
        return `${key}.additionalClassifications`
      }
    }
  })()

  const scheme = (() => {
    switch (param) {
      case 'cav': {
        return ['CAV', 'CAV-PS']
      }
      case 'cpv': {
        return ['CPV', 'CPVS']
      }
      default: {
        return param.toUpperCase()
      }
    }
  })()

  return just({
    type: 'must',
    payload: createClassificationPayload(
      classificationKey,
      scheme,
      ids.map((id) => id.length === 10 ? taxonomifyClassificator(id) : id)
    )
  })
}
