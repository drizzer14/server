import { split } from 'fnts/string'
import { findIndex } from 'fnts/array'
import { compose } from 'fnts/function'

export const taxonomifyClassificator = (id: string): string => {
  return compose(
    findIndex<string>(
      (n, index) => n === '0' && id[index + 1] === '0'
    ),
    split('')
  )(id)
    .foldMap((index) => id.slice(0, index)) as string
}
