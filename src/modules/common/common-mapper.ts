import { nothing } from 'fnts/monad/maybe'

import type { Mapper } from 'services/search/core/entity'

import * as filters from './filters'
import type { CommonParam } from './entity'

export const commonMapper: Mapper<CommonParam> = (context) => {
  switch (context.param) {
    case 'query':
    case 'value': {
      return filters[context.param](
        // @ts-ignore
        context
      )
    }
    case 'item_region': {
      return filters.region(context)
    }
    case 'cav':
    case 'cav-ps':
    case 'cpv':
    case 'cpvs': {
      return filters.classification(context)
    }
    default: {
      return nothing()
    }
  }
}
