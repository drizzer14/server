import type { GetKey } from 'services/search/core/entity'

import type { ProceduresParam } from './entity'

const concat = <F extends string, K extends string>(field: F) => (key: K): `${F}.${K}` =>
  `${field}.${key}` as `${F}.${K}`

const prepend = <F extends string, A extends readonly string[]>(field: F, array: A): Array<`${F}.${A[number]}`> =>
  array.map(concat(field))

export const getProceduresKey: GetKey<ProceduresParam> = (param) => {
  switch (param) {
    case 'status': {
      return 'contractingProcess'
    }
    case 'auctionType':
    case 'stream': {
      return 'contractingProcess.disposalMethodModalities'
    }
    case 'value': {
      return 'contractingProcess.value.amount'
    }
    case 'aid': {
      return 'ocid'
    }
    case 'cav':
    case 'cav-ps':
    case 'cpv':
    case 'cpvs': {
      return 'contractingProcess.items'
    }
    case 'edrpou': {
      return 'parties.identifier.id'
    }
    case 'item_region': {
      return 'contractingProcess.items.propertyAddress'
    }
    case 'date[auction]': {
      return 'contractingProcess.auctionPeriod'
    }
    case 'date[tender]': {
      return 'contractingProcess.tenderPeriod'
    }
    case 'query': {
      return ['awards.buyers.name'].concat(
        prepend(
          'contractingProcess',
          ['title', 'description', 'lots.internalID'].concat(
            prepend('documents', ['title', 'description']),
            prepend(
              'items',
              ['description'].concat(
                prepend(
                  'propertyAddress',
                  ['streetAddress', 'locality', 'region', 'countryName', 'postalCode'].concat(
                    prepend('addressDetails.locality', ['scheme', 'description', 'id'])
                  )
                )
              )
            )
          )
        ),
        prepend(
          'parties',
          ['name'].concat(
            prepend('identifier', ['id', 'legalName']),
            prepend(
              'address',
              ['streetAddress', 'locality', 'region', 'countryName', 'postalCode'].concat(
                prepend('addressDetails.locality', ['scheme', 'description', 'id'])
              )
            ),
            prepend('contactPoint', ['name', 'email', 'telephone', 'faxNumber', 'uri'])
          )
        )
      )
    }
    default: {
      return ''
    }
  }
}
