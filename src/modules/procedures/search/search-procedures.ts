import { createSearchQuery } from 'services/search/core'

import { proceduresMapper } from './procedures-mapper'
import { getProceduresKey } from './get-procedures-key'

export const searchProcedures = createSearchQuery(getProceduresKey, proceduresMapper)
