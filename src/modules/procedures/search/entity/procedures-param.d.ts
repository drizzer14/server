import type { CommonParam } from 'modules/common/entity'

export type ProceduresParam =
  | CommonParam
  | 'stream'
  | 'auctionType'
  | 'aid'
  | 'edrpou'
  | 'date[auction]'
  | 'date[tender]'
