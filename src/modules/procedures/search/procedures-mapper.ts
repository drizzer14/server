import { commonMapper } from 'modules/common'
import { organization } from 'modules/common/filters'
import type { CommonParam } from 'modules/common/entity'
import type { Context, Mapper } from 'services/search/core/entity'

import * as filters from './filters'
import type { ProceduresParam } from './entity'

export const proceduresMapper: Mapper<ProceduresParam> = (context) => {
  switch (context.param) {
    case 'status':
    case 'stream':
    case 'auctionType':
    case 'aid': {
      return filters[context.param](context)
    }
    case 'date[tender]':
    case 'date[auction]': {
      return filters.date(context)
    }
    case 'edrpou': {
      return organization(context)
    }
    default: {
      return commonMapper(context as Context<CommonParam>)
    }
  }
}
