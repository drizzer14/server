import { just, nothing } from 'fnts/monad/maybe'

import { createMustPayload } from 'services/search/core'
import { createStringQuery, createTermQuery } from 'services/search/utils'

export const auctionType = createMustPayload(({ key, value }) => {
  switch (value) {
    case 'english': {
      return just({
        bool: {
          should: [
            createTermQuery(key, ['dgfOtherAssets', 'dgfFinancialAssets', 'propertyLease', 'sellout.english']),
            createStringQuery(key, '*english*'),
          ],
        },
      })
    }
    case 'dutch': {
      return just({
        bool: {
          should: [
            createTermQuery(key, ['dgfInsider', 'appraisal.insider', 'sellout.insider']),
            createStringQuery(key, '*dutch*'),
          ],
        },
      })
    }
    case 'multiAwards': {
      return just(createStringQuery(key, '*multiAwards*'))
    }
    default: {
      return nothing()
    }
  }
})
