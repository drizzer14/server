import {
  createStringQuery,
  createTermQuery,
  joinQueryString,
} from 'services/search/utils'
import { createMustPayload } from 'services/search/core'
import { createClassificationPayload } from 'modules/common/filters/classification'

import { getProceduresKey } from '../get-procedures-key'

export const stream = createMustPayload(({ key, value }) => {
  const acKey = `${getProceduresKey('cav')}.additionalClassifications`
  const edrpouKey = `${getProceduresKey('edrpou')}`

  switch (value) {
    case 'otherAssets': {
      return {
        bool: {
          must: [createTermQuery(key, ['dgfOtherAssets', 'appraisal.insider'])],
          must_not: [
            createClassificationPayload(acKey, 'CAV-PS', 'PA01-7'),
            createClassificationPayload(acKey, 'CPVS', ['QB23-5', 'QB49-3', 'MA08-5']),
          ],
        },
      }
    }
    case 'sellout': {
      return {
        bool: {
          must: createTermQuery(key, ['sellout.english', 'sellout.insider']),
        },
      }
    }
    case 'legitimatePropertyLease': {
      return {
        bool: {
          should: [
            createClassificationPayload(acKey, 'CPVS', 'QB29-3'),
            createStringQuery(key, 'legitimatePropertyLease-*'),
          ],
        },
      }
    }
    case 'propertyLease': {
      return {
        bool: {
          should: [
            createTermQuery(key, 'propertyLease'),
            {
              bool: {
                must: createClassificationPayload(acKey, 'CAV-PS', 'PA01-7'),
              },
            },
          ],
          must_not: createClassificationPayload(acKey, 'CPVS', 'QB29-3'),
        },
      }
    }
    case 'dgf': {
      return {
        bool: {
          should: [
            createTermQuery(key, ['dgfFinancialAssets', 'dgfOtherAssets', 'dgfInsider']),
            createStringQuery(key, joinQueryString(['dgf-english', 'dgf-insider'], 'OR')),
          ],
        },
      }
    }
    case 'bankruptcy': {
      return {
        bool: {
          must: [
            createTermQuery(`${acKey}.scheme`, 'CPVS'),
            {
              match_phrase_prefix: {
                [`${acKey}.id`]: {
                  query: 'QB23-5',
                },
              },
            },
          ],
        },
      }
    }
    case 'timber': {
      return {
        bool: {
          should: [
            {
              bool: {
                must: [
                  createTermQuery(`${acKey}.scheme`, 'CPVS'),
                  {
                    match_phrase_prefix: {
                      [`${acKey}.id`]: {
                        query: 'QB49-3',
                      },
                    },
                  },
                ],
              },
            },
            {
              bool: {
                must: createStringQuery(key, 'timber-*'),
              },
            },
          ],
        },
      }
    }
    case 'railwayCargo': {
      return {
        bool: {
          should: [
            {
              bool: {
                must: [createTermQuery(`${acKey}.scheme`, 'CPVS'), createTermQuery(`${acKey}.id`, 'MA08-5')],
              },
            },
            {
              bool: {
                must: createStringQuery(key, 'railwayCargo-*'),
              },
            },
          ],
        },
      }
    }
    case 'subsoil': {
      return {
        bool: {
          should: [
            {
              bool: {
                must: createStringQuery(edrpouKey, '37536031'),
              },
            },
            {
              bool: {
                must: createStringQuery(key, 'subsoil-*'),
              },
            },
          ],
        },
      }
    }
    case 'landLease': {
      return {
        bool: {
          must: createTermQuery(key, 'landLease'),
        },
      }
    }
    default: {
      return null
    }
  }
})
