import { foldl } from 'fnts/array'
import { split } from 'fnts/string'
import type { Pair } from 'fnts/pair'
import { just } from 'fnts/monad/maybe'

import { createTermQuery } from 'services/search/utils'
import type { Filter } from 'services/search/core/entity'

export const status: Filter = ({ key, value: values }) => {
  const payloads = foldl<string, Pair<string[]>>(
    (target, value) => {
      const [status, statusDetails] = split('.')(value) as [string, string] | [string]

      return statusDetails
        ? [[...target[0], statusDetails], target[1]]
        : [target[0], [...target[1], status]]
    },
    [['statusDetails'], ['status']]
  )(values)

  const lengths = foldl<string[], number>(
    (accumulator, current) => accumulator + (current.length - 1),
    0
  )(payloads)

  return just({
    type: 'must',
    payload: {
      bool: {
        [lengths > 1 ? 'should' : 'must']: foldl<string[], object[]>(
          (target, [term, ...payload]) => {
            return payload.length === 0
              ? target
              : target.concat(createTermQuery(`${key}.${term}`, payload))
          },
          [])(payloads),
      },
    },
  })
}
