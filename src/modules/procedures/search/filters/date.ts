import { createMustPayload } from 'services/search/core'

export const date = createMustPayload(({ key, value }) => {
  const [gte, lte] = value.split('—').map((dateString) => {
    const [day, month, year] = dateString.split('.') as [string, string, string]

    return `${year}-${month}-${day}`
  }) as [string, string]

  return {
    bool: {
      must: [
        {
          range: {
            [`${key}.startDate`]: {
              gte: `${gte}T00:00:00`, // From the start of the same day,
              lte: `${lte}T23:59:59.999Z`, // Till the end of the current day
            },
          }
        },
      ],
    },
  }
})
