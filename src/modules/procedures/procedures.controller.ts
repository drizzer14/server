import { controller } from 'lib/service'

import * as routes from './routes'

export const proceduresController = controller(
  'procedures',
  {
  routes: Object.values(routes)
  }
)
