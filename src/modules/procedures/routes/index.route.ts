import { searchProcedures } from '../search'
import { searchRoute } from '../../common/routes'

export const indexRoute = searchRoute({
  name: 'procedures',
  searcher: searchProcedures
})
