# tiny.prozorro.searcher/server

## Setup

### Development

```shell
λ yarn install

λ yarn start
```

### Production (Docker)

```shell
λ sh ./bin/deploy.sh
```

## Common

Example:

```
?size=10

// size + 10
&offset=10

// [type]:[order]
&sort=date:desc

// contractingProcess.items.propertyAddress.postalCode -> 01000–06999, м. Київ
&item_region=01–06

// contractingProcess.value.amount -> [gte]–[lte]
&value=1000–2000

// Full-text search
&query=Оренда офісних приміщень
```

## Procedures

`/procedures`

Example:

```
// ocid
&aid=UA-PS-2021-04-16-000039-2

// english / dutch / multiAwards
&auctionType=english

// contractingProcess.status & contractingProcess.statusDetails
&status=active.tendering

// otherAssets / sellout / legitimatePropertyLease / dgf / bankruptcy / timber 
   / railwayCargo / subsoil / landLease
&stream=propertyLease

// parties.identifier.id
&edrpou=21560045

// contractingProcess.auctionPeriod.startDate -> [gte]–[lte]
&date[auction]=01.04.2021–02.04.2021

// contractingProcess.tenderPeriod.startDate -> [gte]–[lte]
&date[tender]=01.04.2021–02.04.2021
```

## Registries

`/registries`

Example:

```
// resolutions.disposalMethod -> Second / Undefined
&listType=First

// privatization / lease
&source=dgf

// claimRights
&assetType=basic

// object.status
&status=approved
```
