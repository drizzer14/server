FROM node:12.20.1-alpine3.10

ARG SERVICE_HOST
ARG SERVICE_PORT
ARG ELASTIC_URL

ARG FF_REGISTRIES
ARG FF_PROCEDURES

ENV NODE_ENV=production

WORKDIR /usr/src/tiny.prozorro.searcher

COPY package.json yarn.lock tsconfig.json ./
COPY src ./src

RUN yarn install --frozen-lockfile --production && \
    yarn --cache-clean --force && \
    yarn build

EXPOSE $SERVICE_PORT

CMD yarn serve
