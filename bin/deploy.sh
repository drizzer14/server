docker system prune -f &&

(
  export SERVICE_HOST=0.0.0.0
  export SERVICE_PORT=9090
  export ELASTIC_URL=http://46.4.228.9:9006

  export FF_REGISTRIES=true
  export FF_PROCEDURES=true

  docker-compose up --detach --build
)
